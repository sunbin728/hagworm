python setup.py sdist build
twine upload ./dist/*

rmdir /S /Q dist
rmdir /S /Q build
rmdir /S /Q hagworm.egg-info
# -*- coding: utf-8 -*-

__author__ = r'wsb310@gmail.com'

import pytest

from hagworm.extend.asyncio.base import Utils
from hagworm.extend.asyncio.task import RateLimiter


pytestmark = pytest.mark.asyncio
# pytest.skip(allow_module_level=True)


class TestHTTPClient:

    async def test_rate_limiter(self):

        async def _temp():
            await Utils.sleep(1)
            return True

        limiter = RateLimiter(2, 5, 200)

        _f1 = await limiter.append(_temp)
        _f2 = await limiter.append(_temp)
        _f3 = await limiter.append(_temp)
        _f4 = await limiter.append(_temp)
        _f5 = await limiter.append(_temp)
        _f6 = await limiter.append(_temp)

        await _f1
        await _f2
        await _f3
        await _f4
        await _f5
        await _f6
